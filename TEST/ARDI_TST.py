# -*- coding: utf-8 -*-
#!/usr/bin/env python
#!/usr/bin/python -u

#SIM_IMP_DTPR -> Simulations impedance data process.

import sys
sys.path.append('..')
from PY import ARDI

def TEST():
	print("ARDI_TST")

	print("ARDI FILE OPEN")
	ARD=ARDI.ARDO("Terminal Z Parameter Plot 1.csv","Z")
	print("HEADER")
	print(ARD.HEADER)
	print("OPTIMETRIX")
	print(ARD.OPTC)
	#print("FREQUENCY")
	#print(ARD.FRQ)
	print("IMPEDANCE")
	print(ARD.IMP)


if __name__ == '__main__':
	TEST()
