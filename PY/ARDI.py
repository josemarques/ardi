# -*- coding: utf-8 -*-
#!/usr/bin/env python
#!/usr/bin/python -u

#ARDI.py ->Ansys results data importer
import os
import sys
sys.path.append('..')
from SUBM.CSV import CSV
from SUBM.RFU.PY import IMNT
from SUBM.MATH.PY import LNAL

def AHHP(strline):
	#ANSYS HFSS Header parser
	#Data points between ""
	HEAD=[]
	tHEAD=strline.split('"')
	#print("tHEAD")
	#print(tHEAD)
	for i in range(1,len(tHEAD),2):
		HEAD.append(tHEAD[i])

	#print("HEAD")
	#print(HEAD)

	return HEAD

class ARDO:#Ansys results data object
	def __init__(self, filename="", datatype="Z"):#Data type specifies if the csv report is from Z or S matrices
		self.FILENAME=filename
		self.DATATYPE=datatype

		self.CSVF=CSV.CSV(self.FILENAME,",",1)#Data file. CSV encoding

		self.OPTC=[]#List of simulation cases. Optimetrix combinatory

		#Correlated datasets. Same length
		self.FRQ=[]#Frequency						{
		self.ZMTX=[]#Z matrices						{ FRQ, MTX, OPTMs
		self.SMTX=[]#S matrices						{ FRQ, MTX, OPTMs
		self.OPTM=[]#Optimetrix variables data		{
		#Correlated datasets. Same length

		self.IMP=[]#Impedance data

		self.INIT()

	def FL(self):
		#CSV File load
		self.CSVF.GFD()#CSV object get file data

	def DTL(self):#Data loader
		self.HEADER=[]
		self.PREPDATA=self.CSVF.DATA
		STRHEAD=self.CSVF.STRDATA.splitlines()[0]
		self.HEADER=AHHP(STRHEAD)
		print(self.HEADER)

	def DID(self):
		#Data identifier. Identification of matrices, frequency and optimetrix variables

		#Variable identification
		frqid=0
		optid=[]
		mtxid=[]
		#print(self.HEADER)
		for i in range(len(self.HEADER)):
			#print(self.HEADER[i][1:3])
			if(self.HEADER[i][0:4]=="Freq"):
				frqid=i
			if(self.HEADER[i][0:1]=="$"):#TODO Possible multiple definition types of optimetrix variables
				optid.append(i)
			if(self.HEADER[i][0:2]=="re" or self.HEADER[i][0:2]=="im"):
				mtxid.append(i)

		#print("frqid")
		#print(frqid)
		#print("optid")
		#print(optid)
		#print("mtxid")
		#print(mtxid)
		return frqid, optid, mtxid
		#Variable identification


	def DEX(self,identifiers):#Data extration
		frqid=identifiers[0]
		optid=identifiers[1]
		mtxid=identifiers[2]
		#Variable extraction

		self.FRQ=[]#Frequency						{
		self.SMTX=[]#S matrices						{ FRQ, MTX, OPTMs
		self.ZMTX=[]#Z matrices						{ FRQ, MTX, OPTMs
		self.OPTM=[]#Optimetrix variables data		{

		for i in range(len(self.PREPDATA)):
			#print(self.PREPDATA[i])

			self.FRQ.append(self.PREPDATA[i][frqid]*1e9)

			if(self.DATATYPE=="Z"):

				tZMTX=[]
				for ii in range(len(mtxid)):
					#print(len(self.PREPDATA[i]))
					#print(mtxid[ii])
					tZMTX.append(self.PREPDATA[i][mtxid[ii]])

				#TODO generalization
				self.ZMTX.append(	[[complex(tZMTX[0], tZMTX[1]),complex(tZMTX[4], tZMTX[5])],
									[complex(tZMTX[2], tZMTX[3]), complex(tZMTX[6], tZMTX[7])]])
				#TODO generalization

				#TODO Zmatrix to Smatrix conversion

			elif(self.DATATYPE=="S"):
				tSMTX=[]
				for ii in range(len(mtxid)):
					#print(len(self.PREPDATA[i]))
					#print(mtxid[ii])
					tSMTX.append(self.PREPDATA[i][mtxid[ii]])

				#TODO generalization
				self.SMTX.append(	[[complex(tSMTX[0], tSMTX[1]),complex(tSMTX[4], tSMTX[5])],
									[complex(tSMTX[2], tSMTX[3]), complex(tSMTX[6], tSMTX[7])]])
				self.ZMTX.append(IMNT.SmtZm(LNAL.Mreal(self.SMTX[len(self.SMTX)-1]),LNAL.Mimag(self.SMTX[len(self.SMTX)-1])))
				#TODO generalization

			tOPTM=[]
			for ii in range(len(optid)):
				tOPTM.append(self.PREPDATA[i][optid[ii]])
			self.OPTM.append(tOPTM)


		self.OPTC=[]
		tOPTM=LNAL.TM(self.OPTM)
		for i in range(len(tOPTM)):
			tOPTC=[]
			for ii in tOPTM[i]:
				if ii not in tOPTC:
					tOPTC.append(ii)
			self.OPTC.append(tOPTC)

		#Variable extraction

	def ZCAL(self, contype=3):
		#Impedance calculation.
		self.IMP=[]
		if (contype==3):
			for i in range(len(self.ZMTX)):
				tIMP=IMNT.ZmtZ_12(LNAL.Mreal(self.ZMTX[i]), LNAL.Mimag(self.ZMTX[i]))
				self.IMP.append(complex(tIMP[0],tIMP[1]))
		#print(len(self.ZMTX))
		#print(len(self.IMP))


	def INIT(self):#Initialize simulation data from file
		self.FL()
		self.DTL()
		self.DEX(self.DID())
		self.ZCAL()

	def GSDS(self, optc):#Get specific data set matching opt variable. optc one less dimension that OPTM
		sFRQ=[]#Subset of FRQ
		sIMP=[]#Subset of IMP
		for i in range(len(self.OPTM)):
			if(self.OPTM[i]==optc):
				sFRQ.append(self.FRQ[i])
				sIMP.append(self.IMP[i])

		return sFRQ, sIMP

def main(argv):

	FILES=["../../SIMU/TRANS_A1/TRNS_A1_T0ENV0/Z-TRNS-A1_T0ENV0.csv"]
	SIMULATIONS=[]

	for i in FILES:
		SIMULATIONS.append(ANSYS_SIM(i))

	for i in SIMULATIONS:
		print("HEADER")
		print(i.HEADER)
		ii=10000
		print("FRQ["+str(ii)+"]")
		print(i.FRQ[ii])
		print("OPT["+str(ii)+"]")
		print(i.OPTM[ii])
		print("MTX["+str(ii)+"]")
		print(i.ZMTX[ii])
		print("IMP["+str(ii)+"]")
		print(i.IMP[ii])


		#print("PREPDATA")
		#print(i.PREPDATA)





if __name__ == "__main__":
    main(sys.argv[1:])
